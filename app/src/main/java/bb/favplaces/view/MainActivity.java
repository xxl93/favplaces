package bb.favplaces.view;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import bb.favplaces.R;
import bb.favplaces.databinding.ActivityMainBinding;
import bb.favplaces.viewModel.MainActivityViewModel;

public class MainActivity extends AppCompatActivity {


    ActivityMainBinding mainActivityBinding;
    MainActivityViewModel mainAppViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        initDataBinding();
    }

    public void initDataBinding(){
        mainActivityBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        mainAppViewModel = new MainActivityViewModel(getSupportFragmentManager());
        mainActivityBinding.setVm(mainAppViewModel);
    }
}
