package bb.favplaces.view.Adapter;

import android.databinding.DataBindingUtil;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import bb.favplaces.R;
import bb.favplaces.data.DataHandler;
import bb.favplaces.data.DataManagers.DatabaseStorageManager;
import bb.favplaces.data.DataManagers.InternalStorageManager;
import bb.favplaces.data.Model.Note;
import bb.favplaces.databinding.NoteRowBinding;
import bb.favplaces.utils.TouchHelper;
import bb.favplaces.view.fragment.AddNoteFragment;


public class NoteAdapter extends RecyclerView.Adapter<NoteViewHolder> {

    static NoteAdapter noteAdapter;
    private ItemTouchHelper mItemTouchHelper;
    private DataHandler dataHandler;
    private static List<Note> noteList = new ArrayList<>();

    NoteAdapter(){
        ItemTouchHelper.Callback callback = new TouchHelper();
        mItemTouchHelper = new ItemTouchHelper(callback);
        dataHandler = new DatabaseStorageManager(); //new InternalStorageManager();
    }

    public ItemTouchHelper getTouchHelper(){
        return mItemTouchHelper;
    }

    public static NoteAdapter getInstance(){
        if(noteAdapter == null)
            noteAdapter = new NoteAdapter();

        return noteAdapter;
    }

    public void addNoteToList(Note note){
        noteList.add(note);
        dataHandler.save(note);
        notifyDataSetChanged();
    }

    public void loadData(){
        noteList = dataHandler.load();
        notifyDataSetChanged();
    }

    @Override
    public NoteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        NoteRowBinding noteRowBinding =
                DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.note_row,
                        parent, false);
        return new NoteViewHolder(noteRowBinding);
    }

    @Override
    public void onBindViewHolder(final NoteViewHolder holder, int position) { //Remember to not use position!
        holder.bindNote(noteList.get(holder.getAdapterPosition()));
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                mItemTouchHelper.startDrag(holder);
                return false;
            }
        });

        holder.itemView.findViewById(R.id.imageButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddNoteFragment dialog = AddNoteFragment.newInstance(true, holder.getAdapterPosition());
                dialog.show(((FragmentActivity) v.getContext()).getSupportFragmentManager(), "EditNote");

            }
        });

    }

    @Override
    public int getItemCount() {
        return noteList.size();
    }

    public void onItemMove(int fromPosition, int toPosition) {
        Collections.swap(noteList, fromPosition, toPosition);

        dataHandler.save(noteList);
        notifyItemMoved(fromPosition, toPosition);
    }

    public void onItemDismiss(int position) {
        dataHandler.delete(noteList.get(position));
        noteList.remove(position);
        notifyItemRemoved(position);
    }

    public void editItemOnPos(int position,Note note){
        Note oldNote = noteList.get(position);
        oldNote.update(note);
        dataHandler.update(oldNote);
        
        noteList.set(position,note);
        notifyItemChanged(position);
    }

    public Note getNoteAtPos(int pos){
        return noteList.get(pos);
    }

}

