package bb.favplaces.view.Adapter;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;

import bb.favplaces.data.Model.Note;
import bb.favplaces.databinding.NoteRowBinding;
import bb.favplaces.viewModel.NotesRowViewModel;

public class NoteViewHolder extends RecyclerView.ViewHolder{
    NoteRowBinding mNoteRowBinding;
    static Drawable settings;

    public NoteViewHolder(NoteRowBinding noteRowBinding) {
        super(noteRowBinding.noteRow);
        this.mNoteRowBinding = noteRowBinding;
    }

    public void bindNote(Note note ){
        mNoteRowBinding.setVm(new NotesRowViewModel(note));
    }

    public void onItemSelected() {
        settings = itemView.getBackground();
        itemView.setBackgroundColor(Color.LTGRAY);
    }

    public void onItemClear() {
        itemView.setBackground(settings);
    }
}
