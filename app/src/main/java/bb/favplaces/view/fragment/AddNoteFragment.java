package bb.favplaces.view.fragment;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import bb.favplaces.R;
import bb.favplaces.databinding.AddNoteDialogBinding;
import bb.favplaces.viewModel.AddNoteViewModel;


public class AddNoteFragment extends DialogFragment {

    AddNoteDialogBinding fragmentAddNoteBinding;

    public AddNoteFragment(){}

    public static AddNoteFragment newInstance(boolean b, int pos){
        AddNoteFragment addNoteFragment = new AddNoteFragment();
        Bundle args = new Bundle();
        args.putBoolean("edit", b);
        args.putInt("pos", pos);
        addNoteFragment.setArguments(args);
        return addNoteFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        fragmentAddNoteBinding = DataBindingUtil.inflate(inflater,
                R.layout.add_note_dialog, container, false);

        int pos = getArguments().getInt("pos");
        boolean edit = getArguments().getBoolean("edit");

        fragmentAddNoteBinding.setVm(new AddNoteViewModel(this, edit, pos));
        return fragmentAddNoteBinding.getRoot();
    }

    @Override
    public void onResume(){
        super.onResume();

        fragmentAddNoteBinding.note.requestFocus();
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }

    @Override
    public void dismiss(){
        ((InputMethodManager) getDialog().getContext().getSystemService(Context.INPUT_METHOD_SERVICE))
                .hideSoftInputFromWindow(getView().getWindowToken(), 0);
        super.dismiss();
    }
}
