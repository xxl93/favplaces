package bb.favplaces.view.fragment;


import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;

import bb.favplaces.R;
import bb.favplaces.databinding.FragmentLocationBinding;
import bb.favplaces.viewModel.LocationViewModel;

public class LocationFragment extends Fragment {

    FragmentLocationBinding fragmentLocationBinding;
    LocationViewModel locationViewModel;
    FusedLocationProviderClient fusedLocationProviderClient;

    final int MY_PERMISSIONS_REQUEST_LOCATION = 123;

    public LocationFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        fragmentLocationBinding = DataBindingUtil.inflate(inflater,
                R.layout.fragment_location, container, false);

        checkPermission(getActivity());

        fusedLocationProviderClient
                = LocationServices.getFusedLocationProviderClient(getActivity());
        locationViewModel = new LocationViewModel(fusedLocationProviderClient);

        fragmentLocationBinding.setVm(locationViewModel);

        return fragmentLocationBinding.getRoot();
    }


    public void checkPermission(Activity activity) {
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                ) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    MY_PERMISSIONS_REQUEST_LOCATION);
        }
    }

    @SuppressWarnings("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[],@NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                if (grantResults.length > 0
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                }
                return;
            }
        }
    }
}
