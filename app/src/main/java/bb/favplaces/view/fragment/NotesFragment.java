package bb.favplaces.view.fragment;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import bb.favplaces.R;
import bb.favplaces.databinding.FragmentNotesBinding;
import bb.favplaces.view.Adapter.NoteAdapter;
import bb.favplaces.viewModel.NotesViewModel;

public class NotesFragment extends Fragment {

    FragmentNotesBinding fragmentNotesBinding;
    NotesViewModel notesViewModel;
    NoteAdapter noteAdapter;

    public NotesFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        fragmentNotesBinding = DataBindingUtil.inflate(inflater,
                R.layout.fragment_notes, container, false);
        setupAdapter();

        notesViewModel = new NotesViewModel(noteAdapter);

        fragmentNotesBinding.setVm(notesViewModel);
        return fragmentNotesBinding.getRoot();
    }

    private void setupAdapter() {
        noteAdapter = NoteAdapter.getInstance();

        noteAdapter.getTouchHelper().attachToRecyclerView(fragmentNotesBinding.list);
        fragmentNotesBinding.list.setAdapter(noteAdapter);
        fragmentNotesBinding.list.setLayoutManager(new LinearLayoutManager(getContext()));
    }
}
