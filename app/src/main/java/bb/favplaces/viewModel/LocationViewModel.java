package bb.favplaces.viewModel;

import android.Manifest;
import android.content.pm.PackageManager;
import android.databinding.ObservableField;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.support.v4.app.ActivityCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;

import java.io.IOException;
import java.util.List;


public class LocationViewModel {

    public ObservableField<String> note = new ObservableField<>("Note");
    public ObservableField<String> loc = new ObservableField<>("Please enable your GPS to get localization");
    public static String locationNote = "";


    private FusedLocationProviderClient mFusedLocationClient;


    public LocationViewModel(FusedLocationProviderClient fusedLocationProviderClient) {
        mFusedLocationClient = fusedLocationProviderClient;

        LocationRequest locationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10)
                .setFastestInterval(10);

        if (ActivityCompat.checkSelfPermission(fusedLocationProviderClient.getApplicationContext(),
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(fusedLocationProviderClient.getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        fusedLocationProviderClient.requestLocationUpdates(locationRequest, mLocationCallback, null);
    }


    public String getLocation(){
        return loc.get();
    }

    LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            for (Location location : locationResult.getLocations()) {
              //loc.set(new LatLng(location.getLatitude(), location.getLongitude()).toString());

                Geocoder geocoder = new Geocoder(mFusedLocationClient.getApplicationContext());
                try {
                    List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 2);

                    locationNote =addresses.get(0).getAddressLine(0);
                    if(locationNote.equals(""))
                        locationNote = "Please enable your GPS to get localization";
                    loc.set(locationNote);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    };
}
