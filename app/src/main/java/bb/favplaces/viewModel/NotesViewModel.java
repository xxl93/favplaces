package bb.favplaces.viewModel;


import android.databinding.BaseObservable;

import bb.favplaces.view.Adapter.NoteAdapter;

public class NotesViewModel extends BaseObservable{

    public NotesViewModel(NoteAdapter noteAdapter){
        NoteAdapter mNoteAdapter = noteAdapter;
        mNoteAdapter.loadData();
    }
}
