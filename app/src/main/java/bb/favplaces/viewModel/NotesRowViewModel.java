package bb.favplaces.viewModel;

import bb.favplaces.data.Model.Note;

public class NotesRowViewModel {
    private Note mNote;
    public NotesRowViewModel(Note note){
        mNote = note;
    }

    public String getLocation(){
        return mNote.location;
    }
    public String getNote(){
        return mNote.note;
    }
    public void setNote(Note note){
        mNote = note;
    }
}
