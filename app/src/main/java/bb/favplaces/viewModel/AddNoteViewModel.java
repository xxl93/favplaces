package bb.favplaces.viewModel;

import android.view.View;
import android.widget.Toast;

import bb.favplaces.MyApp;
import bb.favplaces.data.Model.Note;
import bb.favplaces.view.Adapter.NoteAdapter;
import bb.favplaces.view.fragment.AddNoteFragment;

public class AddNoteViewModel {

    public String location = "";
    public  String note = "";
    AddNoteFragment df;
    boolean edit = false;
    int position = -1;

    public AddNoteViewModel(AddNoteFragment dialogFragment, boolean editNote, int pos){
        location = LocationViewModel.locationNote;
        df = dialogFragment;

        position = pos;
        edit = editNote;


        Note edit;
        if(position >=0) {
            edit = NoteAdapter.getInstance().getNoteAtPos(position);
            location = edit.location;
            note = edit.note;
        }
    }

    public View.OnClickListener acceptButton = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(edit){
                Note newNote = new Note(location, note);
                NoteAdapter.getInstance().editItemOnPos(position, newNote);
                //NoteAdapter.getInstance().addNoteToList(newNote);
                df.dismiss();
                return;
            }

            if(fieldsNotEmpty()) {
                Note newNote = new Note(location, note);
                NoteAdapter.getInstance().addNoteToList(newNote);
                df.dismiss();
            }
        }
    };

    public View.OnClickListener closeButton = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            df.dismiss();
        }
    };

    public boolean fieldsNotEmpty(){
        if(location.equals("") || note.equals("")){
            Toast.makeText(MyApp.getContext(), "Note and location cannot be empty", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

}
