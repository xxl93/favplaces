package bb.favplaces.viewModel;

import android.databinding.BindingAdapter;
import android.support.design.widget.TabLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.View;

import bb.favplaces.utils.CustomViewPager;
import bb.favplaces.view.Adapter.SectionsPagerAdapter;
import bb.favplaces.view.fragment.AddNoteFragment;

public class MainActivityViewModel {


    public static SectionsPagerAdapter mSectionsPagerAdapter;
    public static CustomViewPager mCustomViewPager;
    FragmentManager f;

    public MainActivityViewModel(FragmentManager fm){
        mSectionsPagerAdapter = new SectionsPagerAdapter(fm);
        f = fm;
    }

    public View.OnClickListener addNote = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            DialogFragment newFragment = AddNoteFragment.newInstance(false,-1);
            newFragment.show(f , "New dialog");
        }

    };

    @BindingAdapter({"adapter"})
    public static void bindAdapter(CustomViewPager viewPager, SectionsPagerAdapter sectionsPagerAdapter){
        viewPager.setAdapter(sectionsPagerAdapter);
        mCustomViewPager = viewPager;
        mCustomViewPager.setPagingEnabled(false);
    }

    @BindingAdapter({"viewPager"})
    public static void setupViewPager(TabLayout tabLayout, ViewPager viewPager){
        tabLayout.setupWithViewPager(mCustomViewPager);
    }
}
