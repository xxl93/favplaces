package bb.favplaces.data;

import java.util.List;

import bb.favplaces.data.Model.Note;

public interface DataHandler {
    public void save(List<Note>  noteList);
    public void save(Note note);
    public List<Note> load();
    public void update(Note note);
    public void delete(Note note);
}
