package bb.favplaces.data.DataManagers;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.Observer;
import android.os.AsyncTask;
import android.support.annotation.Nullable;

import com.google.android.gms.gcm.Task;

import java.util.ArrayList;
import java.util.List;

import bb.favplaces.MyApp;
import bb.favplaces.data.DataHandler;
import bb.favplaces.data.Model.Note;

public class DatabaseStorageManager implements DataHandler {

    private NoteDatabase noteDatabase;
    private MediatorLiveData<List<Note>> noteLive = new MediatorLiveData<>();
    public DatabaseStorageManager(){
        noteDatabase = NoteDatabase.getDatabase(MyApp.getContext());
    }

    @Override
    public void save(List<Note> noteList) {
        for(Note n :noteList){
            noteDatabase.notesModelDao().addNote(n);
        }
    }

    @Override
    public void save(Note note) {
        noteDatabase.notesModelDao().addNote(note);
    }

    @Override
    public List<Note> load() {
        //AsyncTask is not needed if we have LiveData but to hold both classes in
        // interfaces correct it should be used.
        // Anyway In that case it will be done in main thread.
        //final LiveData<List<Note>> t = noteDatabase.notesModelDao().getAllNotes();
        List <Note> noteList = noteDatabase.notesModelDao().getAllNotes();
        return noteList;
    }

    @Override
    public void update(Note note) {
        noteDatabase.notesModelDao().editNote(note);
    }

    @Override
    public void delete(Note note) {
        noteDatabase.notesModelDao().deleteNote(note);
    }
}
