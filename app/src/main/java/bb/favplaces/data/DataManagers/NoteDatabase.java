package bb.favplaces.data.DataManagers;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import bb.favplaces.NoteModelDao;
import bb.favplaces.data.Model.Note;


@Database(entities = {Note.class}, version = 1)
public abstract class NoteDatabase extends RoomDatabase{

    private static NoteDatabase INSTANCE;
    public abstract NoteModelDao notesModelDao();

    public static NoteDatabase getDatabase(Context context){
        if(INSTANCE == null){
            INSTANCE = Room
                    .databaseBuilder(context.getApplicationContext(), NoteDatabase.class, "notes_db")
                    .allowMainThreadQueries()
                    .build();
        }
        return INSTANCE;
    }
}
