package bb.favplaces.data.DataManagers;

import android.content.Context;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import bb.favplaces.MyApp;
import bb.favplaces.R;
import bb.favplaces.data.DataHandler;
import bb.favplaces.data.Model.Note;

import static android.content.Context.MODE_APPEND;

public class InternalStorageManager implements DataHandler {

    private Context mContext;
    private File mFile;
    FileOutputStream fos = null;

    public InternalStorageManager(){
        mContext = MyApp.getContext();
        mFile = new File(mContext.getFilesDir(),mContext.getText(R.string.internalDir).toString());
    }

    @Override
    public void save(List<Note> noteList){
        try
        {
            fos = new FileOutputStream(mFile);
            for (Note n : noteList) {
                fos.write(n.saveToTXTFormat().getBytes());
            }
            fos.close();
        }catch (IOException e){}
    }
    @Override
    public void save(Note note){
        try{
            fos = mContext.openFileOutput(mFile.getName(), MODE_APPEND);
            fos.write(note.saveToTXTFormat().getBytes());
            fos.close();
        }catch (IOException e){}
    }
    @Override
    public List<Note> load() {
        List<Note> noteList = new ArrayList<>();

        try {
            InputStreamReader isr =
                    new InputStreamReader(mContext.openFileInput(mFile.getName()));
            BufferedReader bufferedReader = new BufferedReader(isr);

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                noteList.add(Note.getFromTXTFormat(line));
            }
            isr.close();
            bufferedReader.close();
        }catch (IOException e){}
        return noteList;
    }

    @Override
    public void update(Note note) {

    }

    @Override
    public void delete(Note note) {

    }

}
