package bb.favplaces.data.Model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Ignore;

@Entity
public class Note{

    @PrimaryKey(autoGenerate = true)
    public Integer id;
    public String location;
    public String note;

    public Note(String location, String note) {
        this.location = location;
        this.note = note;
    }

    public String getLocation() {
        return location;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note){
        this.note = note;
    }

    private static String delimiter = ";";

    @Ignore
    public String saveToTXTFormat(){
        String sb = location +
                delimiter +
                note +
                "\n";
        return sb;
    }

    @Ignore
    public void update(Note copy){
        this.location = copy.getLocation();
        this.note = copy.getNote();
    }

    @Ignore
    public static Note getFromTXTFormat(String s){
        String [] strings = s.split(delimiter);
        return strings.length > 1 ? new Note(strings[0], strings[1]) : new Note("","");
    }
}
