package bb.favplaces;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import bb.favplaces.data.Model.Note;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface NoteModelDao {
    @Query("select * from Note")
    //LiveData<List<Note>>
    List<Note> getAllNotes();

    @Query("select * from Note where id = :id")
    Note getItemById(String id);

    @Insert(onConflict = REPLACE)
    void addNote(Note... note);

    @Update(onConflict = REPLACE)
    void editNote(Note note);

    @Delete
    void deleteNote(Note note);
}
