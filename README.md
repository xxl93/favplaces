# README #

### How do I get set up? ###

Clone and install gradle packages.

### Content ###

**Architecture**
MVVM

**Setting permissions**

Location : Manifest and LocationFragment.
	
	:::XML
	<uses-permission android:name="android.permission.ACCESS_FINE_LOCATION"/>
    <uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION"/>
    <application

**Getting Location**

Location: LocationFragment, LocationViewModel.

	:::JAVA
	LocationRequest locationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10)
                .setFastestInterval(10);
	fusedLocationProviderClient.requestLocationUpdates(locationRequest, mLocationCallback, null);

**Saving in permament way**

Location: NoteAdapter -> InternalStorageManager. 

Should be done with storage interface to handle multiple ways of handle data.
In this implementation You can see simple txt file. It can be upgraded with parcelable.
But with best aproach database should be used. (testst > db).

Open file, R.string.internalDir is file const name:
	
	:::java
	 mFile = new File(mContext.getFilesDir(), mContext.getText(R.string.internalDir).toString());
	 
Save file

	:::java
	  public void save(List<Note> noteList){
        try
        {
            fos = new FileOutputStream(mFile);
            for (Note n : noteList) {
                fos.write(n.saveToTXTFormat().getBytes());
            }
            fos.close();
        }catch (IOException e){}
    }

    public void save(Note note){
        try{
            fos = mContext.openFileOutput(mFile.getName(), MODE_APPEND);
            fos.write(note.saveToTXTFormat().getBytes());
            fos.close();
        }catch (IOException e){}
    }
	
Load file

	:::java
	 public List<Note> load() {
        List<Note> noteList = new ArrayList<>();

        try {
            InputStreamReader isr =
                    new InputStreamReader(mContext.openFileInput(mFile.getName()));
            BufferedReader bufferedReader = new BufferedReader(isr);

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                noteList.add(Note.getFromTXTFormat(line));
            }
            isr.close();
            bufferedReader.close();
        }catch (IOException e){}
        return noteList;

**Page view - Create custom view pager**

Location: SectionPagerAdapter, CustomViewPager

It is created for using drag and drop in pleasent way. 
It give possibility to block tab switch swipe, simple override base methods.

	:::java
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return this.enabled && super.onTouchEvent(event);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        return this.enabled && super.onInterceptTouchEvent(event);
    }

**Focus edit field and manage keybord at dialog**

Location: AddNoteFragment.
	
	:::java
    @Override
    public void onResume(){
        super.onResume();
        fragmentAddNoteBinding.note.requestFocus();
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }

    @Override
    public void dismiss(){
        ((InputMethodManager) getDialog().getContext().getSystemService(Context.INPUT_METHOD_SERVICE))
                .hideSoftInputFromWindow(getView().getWindowToken(), 0);
        super.dismiss();
    }

**Managing items**

Location NoteAdapter

### Manage items ###

**Adding items**

Location: AddNoteViewModel(DialogFragment)

	:::java
    public View.OnClickListener addNote = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            DialogFragment newFragment = AddNoteFragment.newInstance(false,-1);
            newFragment.show(f , "New dialog");
        }
    };

**Removing, and drag items**

Location: TouchHelper

Working in that way, core:
1) Gets movement flags -> Up|Down|Start|End
2) Remove on swipe	

	:::java
	@Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
      NoteAdapter.getInstance().onItemDismiss(viewHolder.getAdapterPosition());
    } 
	
3)	Drag and drop
	
	:::java
    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder source, RecyclerView.ViewHolder target) {
        if (source.getItemViewType() != target.getItemViewType()) {
            return false;
        }
        NoteAdapter.getInstance().onItemMove(source.getAdapterPosition(), target.getAdapterPosition());
        return true;
    }
	

**Edit items**

Location: AddNoteViewModel

		:::java
        holder.itemView.findViewById(R.id.imageButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddNoteFragment dialog = AddNoteFragment.newInstance(true, holder.getAdapterPosition());
                dialog.show(((FragmentActivity) v.getContext()).getSupportFragmentManager(), "EditNote");

            }
        });

### TODO ###

Create storage interface and factory, add db to inprove data managing.

Testing.

Google Map.